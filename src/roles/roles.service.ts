import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Role } from './role.entity';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>,
  ) { }

  findAll(): Promise<Role[]> {
    return this.rolesRepository.find();
  }

  async seedRoles() {
    const roles = await this.findAll();
    if (roles.length === 0) {
      const adminRole: Role = new Role();
      adminRole.name = 'Admin';

      const userRole: Role = new Role();
      userRole.name = 'User';

      await this.rolesRepository.save([adminRole, userRole]);
    }
  }
}
