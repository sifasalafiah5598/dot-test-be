import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { Role } from '../roles/role.entity';
import * as bcrypt from 'bcryptjs';
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>,
  ) {}

  create(user: User): Promise<User> {
    return this.usersRepository.save(user);
  }

  findAll(): Promise<User[]> {
    return this.usersRepository.find({ relations: ['roles'] });
  }

  findOne(id: any): Promise<User> {
    return this.usersRepository.findOne({ where: { id }, relations: ['roles'] });
  }

  findOneByEmail(email: string): Promise<User> {
    return this.usersRepository.findOne({ where: { email }, relations: ['roles'] });
  }

  async update(id: number, user: Partial<User>): Promise<void> {
    delete user['roles'];
    await this.usersRepository.update(id, user);
  }

  async remove(id: number): Promise<void> {
    await this.usersRepository.delete(id);
  }

  async seedUsers() {
    const dummyUser = await this.findOneByEmail('dummy@mail.com');
    if (!dummyUser) {
      const user: User = new User();
      user.email = 'dummy@mail.com';
      user.name = 'Dummy';
      user.password = 'password';
      const salt = await bcrypt.genSalt(10);
      const hashedPassword = await bcrypt.hash(user?.password ?? user?.email, salt);
      user.password = hashedPassword;
      this.create(user);
    }
  }
  
}
