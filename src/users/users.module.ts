import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { User } from './user.entity';
import { Role } from 'src/roles/role.entity';
import { RolesModule } from 'src/roles/roles.module';

@Module({
    imports: [TypeOrmModule.forFeature([User, Role]), RolesModule],
    providers: [UsersService],
    controllers: [UsersController],
    exports: [UsersService],
})
export class UsersModule {
    constructor(private readonly userService: UsersService) {
        this.userService.seedUsers();
    }
}
