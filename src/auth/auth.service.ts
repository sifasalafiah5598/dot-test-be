import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { UsersService } from '../users/users.service';
import { User } from '../users/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByEmail(email);
    if (user && bcrypt.compareSync(pass, user.password)) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(req: any) {
    const user = await this.validateUser(req.email, req.password);
    if (user) {
      const payload = { email: user.email, sub: user.id };
      return {
        status: true,
        access_token: this.jwtService.sign(payload),
      };
    } else {
      return {
        status: false,
        message: 'Email atau password salah'
      }
    }
    
  }

  async register(user: User): Promise<User> {
    const salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(user.password, salt);
    return this.usersService.create(user);
  }
}
