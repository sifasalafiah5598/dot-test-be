import { Controller, Request, Post, UseGuards, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from '../users/user.entity';

interface LoginDTO {
  email: string,
  password: string
}

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  async login(@Body() req: LoginDTO) {
    return this.authService.login(req);
  }

  @Post('register')
  async register(@Body() user: User) {
    return this.authService.register(user);
  }
}
